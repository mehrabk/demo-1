import { React } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {increment, decrement} from './actions/counterAction';
import {login, logout} from './actions/isLoggedAction';


function App() {
  
  const counter = useSelector(state => state.counter)
  const isLogged = useSelector(state => state.isLogged)

  const dispatch = useDispatch();

  return (
    <div className="App">
      
      <button onClick = {() => dispatch(increment(2))} > + </button>
      <button onClick = {() => dispatch(decrement(2))} > - </button>  <br/><br/>
      
      {counter}  <br/><br/>
      
      <button onClick = {() => dispatch(login())} >Click To Login</button>
      <button onClick = {() => dispatch(logout())} >Click To Logout</button>  <br/><br/>
      
      {isLogged ? 'Welcome To My Page' : ' You Are Logged Out'}

    </div>
  );
}

export default App;